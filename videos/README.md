## Video sequences

This folder contains 2 video sequences taken in the courtyard with a camera rig composed of 1 main camera (SDI) and 2 witness cameras (WC).  


## Calibration of each camera

The camera has been calibrated using the calibration binary `openMVG_main_cameraCalibration` that can be found in the POPART fork of openMVG and that is loosely based on the OpenCV calibration sample program that comes with the library.  The intrinsics parameters are stocked in the `intrinsics*.cal.txt` files of this directory in a format required by the localization script `openMVG_main_rigLocalizer` of OpenMVG. The format is a quite simple plain text collecting one parameter value per row in this order:

```
 int #image width
 int #image height
 double #focal
 double #ppx principal point x-coord
 double #ppy principal point y-coord
 double #k0 first distortion parameter
 double #k1
 double #k2
```

## Calibration of the rig

The camera rig has been calibrated using the calibration tool `openMVG_main_rigCalibration` that can be found in the POPART fork of openMVG.  The extrinsics parameters are stocked in the `rigCalibration.txt` file of this directory in a format required by the localization script `openMVG_main_rigLocalizer` of OpenMVG. The format is a quite simple plain text collecting one parameter value per row in this order:

```
int # number of withness cameras, the main camera is supposed to be the main reference system
double R[0][0] // first camera rotation
double R[0][1]
...
double C[0] // first camera center
double C[1]
double C[2]
double R[0][0] // second camera rotation
...
```

## Convert to image sequence

The localization script `openMVG_main_rigLocalizer` supports as input either a video file or a directory containing the images (more inputs are actually supported, please see the documentation of the script for more information). Since OpenCV is used to read the video, sometimes to avoid rebuilding OpenCV to support the needed codecs, it is easier to convert the video in a sequence of frames. This can be easily done with tools like `ffmpeg`:

```
ffmpeg -i input.mp4 img.%04d.png
```

The above command saves the frames of the input video `input.mp4` into a list of numbered images, i.e. `img.0001.png` for the first frame, `img.0002.png` for the second and so on. The `%04d` is used to add 0s as padding (up to 4) in the frame number. I strongly suggest this format with 4 figures and the dot after the text `img` as this pattern is easily recognized by Maya, if you need to import the result of your tracking and load the image planes associated to the animated camera.

In that case `openMVG_main_rigLocalizer` supports the following folder organization: everything can be placed in folder contain a folder for each camera composing the rig, 
numbered from 0 to N. Inside each folder there must be the intrinsics parameters 
of the camera and the images to be used for the calibration. The images must be 
numbered so that they can be read as synchronized image pairs, ie the first image 
read in each directory must refer to the same acquisition timestamp and so on.
Here is an example of a possible hierarchy of directories:

```
mediapath
  |_0
  | |_ intrinsics.txt
  | |_ image0000.jpg
  | |_ image0001.jpg
  | 
  |_1
  | |_ intrinsics.txt
  | |_ image_0000.png
  |
  ...
```
so that `image0000.jpg` and `image_0000.png` refers to the images taken at the same time stamp.

`intrinsics.txt` is a plain text file containing the internal camera parameters (the `intrinsics*.cal.txt` files)

## References

 - OpenMVG POPART fork: https://github.com/poparteu/openMVG
 - OpenMVG camera calibration tool https://github.com/poparteu/openMVG/blob/popart_develop/src/software/RigCalibration/main_cameraCalibration.cpp 
 - OpenMVG rig calibration tool https://github.com/poparteu/openMVG/blob/popart_develop/src/software/RigCalibration/main_rigCalibration.cpp
 - OpenMVG rig localization tool https://github.com/poparteu/openMVG/blob/popart_develop/src/software/Localization/main_rigLocalizer.cpp
 - OpenCV libraries: http://opencv.org/
 - Original OpenCV calibration sample: https://github.com/Itseez/opencv/blob/master/samples/cpp/calibration.cpp 
 - OpenCV calibration doc: http://docs.opencv.org/3.1.0/d4/d94/tutorial_camera_calibration.html#gsc.tab=0