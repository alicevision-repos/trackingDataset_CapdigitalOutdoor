# Cap Digital Courtyard camera tracking dataset

![](capdigitalCourtyard.png)


## The dataset
This dataset collects 197 images taken in courtyard of [Capdigital building](https://goo.gl/maps/1fAw1KqTp9u) in Paris (FRA) and 2 video sequences taken with a camera rig composed of 1 main camera and 2 witness cameras on the side looking outwards. The aim of the dataset is to test and evaluate the camera tracking algorithms developed for the [POPART](http://www.popartproject.eu/) project, and help the reproducibility of the experiments.

The camera tracking algorithms developed for the POPART project are based on model tracking of an existing 3D reconstruction of the scene. First a collection of still images are taken and a SfM pipeline is used to perform the 3D reconstruction of the scene. As result of this first step, a 3D point cloud is generated. The camera tracking algorithms are then based on camera localization techniques: each frame is individually localized w.r.t. the 3D point cloud using the photometric information (SIFT features) associated to each point. This allows to align the point cloud to the current frame and thus compute the camera pose. 


## Dataset Structure
The dataset is composed of the following directories:

 - `images` contains the 197 original images;
 - `videos` contains the 2 sequences that has been used to test the tracking algorithms for a camera rig along with their calibration files

Check the different `README.md` inside each directory for further information on how the data has been generated or how to reproduce the results.

## License 

This dataset is released under the Creative Commons Attribution-ShareAlike 3.0 Unported license (see [LICENSE.md](LICENSE.md))

## References

 - The POPART project: http://www.popartproject.eu
 - OpenMVG library (POPART fork): https://github.com/poparteu/openMVG 
 - Alembic open computer graphics interchange framework: http://www.alembic.io/
 - Maya® 3D animation, modeling, simulation, and rendering software: http://www.autodesk.com/products/maya/overview

## Citation

If you are referring to or using this dataset please cite it as 

```
Gasparini, Simone and Castan, Fabien. (2016). Capdigital courtyard camera tracking dataset. Zenodo. doi:10.5281/zenodo.59370
```

BibTex
```
@misc{Gasparini2016Capdigital,
  author = {Gasparini, Simone and Castan, Fabien},
  title = {Capdigital courtyard camera tracking dataset},
  month = aug,
  year = 2016,
  doi = {10.5281/zenodo.59370},
  url = {http://dx.doi.org/10.5281/zenodo.59370}
}
```

## Acknowledgements
This dataset has been developed during the [POPART](http://www.popartproject.eu/) project funded by the European Union’s Horizon 2020 research and innovation programme under grant agreement No 644874.

## Author

Simone Gasparini (simone.gasparini@gmail.com)
Fabien Castan